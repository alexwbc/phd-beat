extends Area2D

var player
var deep
var cycles = 3

var timeout#
var lifeanim#play as long player animation is this (nullify timeout)
var origin

var strength = 1 #how much power deal the blow
var crash = false#force the target to crash on ground true/false


var z_deep#actually the y value of the parent


func vanish():
	if player.thrust == self:
		player.thrust = null
	queue_free()
	

func _enter_tree():

	if player.thrust != null:
		player.thrust.vanish()
	player.thrust = self
	global_position = origin.global_position

func _process(delta):
	global_position = origin.global_position
	if player.status == player.statedb["walking"]:
		vanish()
	if timeout >0:
		timeout -= 1*delta
	else:
		vanish()
		return
	if lifeanim != null:
		if player.anim != lifeanim:
			vanish()
			return

	for area in get_overlapping_areas():
		var target = area.get_parent()
		var warn_player = false
		if target.has_meta("enemy"):
			var z_distance = abs(z_deep-target.z_index)
			if z_distance < 50:
				warn_player = true
				target.get_hit(player,"punch", strength, crash)
		if warn_player:
			player.has_hit = true
			player.combo_timer[0] = player.combo_timer[1]
			vanish()
