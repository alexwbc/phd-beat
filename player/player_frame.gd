extends KinematicBody2D


#nodes
var sprite


#hit "bullets"
var punch
var hit
var punch_request = false
var thrust#only one thrust at time can be. if not null (mamaged externally by hit.scn)

var has_hit = false#used for determine combos
var combo_timer = [0,0.3]
#var combo_timer = [0,1]
var combo_count = 0
const MAX_COMBO = 5

#config
const WALK_SPEED = 300
#const MIN_MOVE = 10
const MIN_MOVE = 10
const MIN_RUN = 1000
const RUN_SPEED = 1300
const DEADZONE = 0.3
const GRAVITY = 100
const MAX_GRAVITY = 1300
const JUMP_STR = -2000

#run mechanics
const DOUBLETAP_DELAY = 0.3#range time where doubletap is valid
var run_action = false#"signal" for when  run is possible (horizontal pad return to 0 after first tap)
var run_stop_inertia = 0

#input control
var pad_hor = 0
var pad_ver = 0


#interaction with enemies
var enemy_ring
var enemy_ring_out


#status control
const statedb ={"walking":0
				,"punch":1
				,"jump":2
				,"air_attack":3
				,"run":4
				,"run-jump":5
				,"run-smash":6

				,"gothit-ground":7
				,"gothit-air":8

				}
var status = 0

#var was_running = false

#animation control
var anim = "idle"
var new_anim = "idle"
var lv = Vector2()
var old_pos = Vector2(0,0)
var cur_pos = Vector2(0,0)
var flip_old = true
var flip_new = false

var input_old

#AI interaction
var target


#jump control
#const jump_str = -700
#const gravity = 700
var jump_gravity = 0
var falling = false
var jump_landground = 0#jump_landoff
var jump_y_origin = 0
var jump_linear_inertia = 0
var old_z = 0
#debug
var label




#services (function and processes used to by other nodes)
var enemy_check_count = 0

func _enter_tree():
	punch = load("res://player/hit.tscn")
	hit =  load("res://player/hit.tscn")
#	if base.player != null:
#		base.player.queue_free()
	set_meta("player",0)
	base.player = self
	sprite = $sprite
	label = $camera/Label
	target = $target
	
	#enemy interacitions nodes
	#enemy_ring curve = where enemy place themselves before attack
	enemy_ring = get_node("patterns/enemy_ring").get_curve()
	enemy_ring_out = get_node("patterns/enemy_ring_outro").get_curve()
	




func service_enemy_close():#check and warn enemy who are close to me (list is distribuited by process cycle)
	var enemies = get_tree().get_nodes_in_group("enemies")
	if enemies.size() <= 0:
		return
	enemy_check_count += 1
	if enemy_check_count > (enemies.size()-1):
		enemy_check_count = 0
	var enemy = enemies[enemy_check_count]
	if enemy == null:
		return
	var distance = enemy.global_position-global_position
	enemy.target_distance = distance

func _input(event):
	var directionevent = false
	if (event is InputEventJoypadButton) or (event is InputEventJoypadMotion):
		directionevent = true
		pad_hor = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
		pad_ver = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	if event is InputEventKey:
		directionevent = true
		pad_hor = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
		pad_ver = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")

	if !directionevent:
		return
	var pad_hor_move = abs(pad_hor) > 0.3
	if pad_hor_move and $doubletap.is_stopped():#the first hit
		$doubletap.start(DOUBLETAP_DELAY)
		input_old = pad_hor > 0
		run_action = false


func give_enemy_spot(enemy_pos = null, proximity = 0.5):#find a nice spot for enemy to approach inner ring
	if enemy_pos == null:
		#var camerapos = get_viewport().get_camera().get_camera_position()
		
		#var camerapos = $camera.get_camera_screen_center()
		#print(camerapos)
		
		enemy_pos = get_global_mouse_position()
	
	
	enemy_pos -= global_position#convert enemy position relative to me
	var pos_a = enemy_ring.get_closest_point(enemy_pos)+global_position
	var pos_b = enemy_ring_out.get_closest_point(enemy_pos)+global_position
#	base._debug_shotmark(enemy_ring.get_closest_point(enemy_pos)+global_position)
#	return enemy_ring.get_closest_point(enemy_pos)+global_position
#	base._debug_shotmark(pos_a.slerp(pos_b, proximity))
#	print("slerp from: "+ str(pos_a)+ " and " + str(pos_b)+ " is: " +str(pos_a.linear_interpolate(pos_b, 0.5)))
#	base._debug_shotmark(pos_a.slerp(pos_b, 0.5))
#	return pos_a.slerp(pos_b, proximity)
	return pos_a.linear_interpolate(pos_b, proximity)
#	var point = enemy_ring.get_closest_point(enemy_pos)+global_position
#	print(enemy_pos)
#	base._debug_shotmark(enemy_pos)
#	base._debug_shotmark(point)
#	base._debug_shotmark(enemy_ring.interpolatef(point))
	

func check_direction():

	if abs(lv.length()) < MIN_MOVE:
		return null 
	var walk_dir = lv.normalized()
	var dir = "side"
#	print(walk_dir)
	if walk_dir.x > 0.1:
		flip_new = false
	elif walk_dir.x < -0.1:
		flip_new = true

	if walk_dir.y > DEADZONE:
		dir = str(dir,"dn")
	elif walk_dir.y < -DEADZONE:
		dir = str(dir,"up")
	return dir




func proc_walk():
	
	if $anim.current_animation == "run-stop":
		return
	
	var direction = check_direction()
	if direction == null:
#		print("idle receiverd")
		new_anim = "idle"
	elif direction == "side":
		new_anim = "walk-side"
	elif direction.ends_with("up"):
		new_anim = "walk-sideup"
	elif direction.ends_with("dn"):
		new_anim = "walk-sidedn"


	if Input.is_action_just_pressed("punch"):
		status = statedb["punch"]
		punch_request = true

	if !$doubletap.is_stopped():#first 0, then 
		if !run_action and (abs(pad_hor) < 0.2):
			run_action = true
		elif run_action and (abs(pad_hor) > 0.2):# and (input_old == (pad_hor > 0):
			if input_old == (pad_hor > 0):
				$doubletap.stop()
				lv.x = MIN_RUN+1
				status = statedb["run"]

				new_anim = "run"
			else:
				$doubletap.stop()
				run_action = false



	if anim != new_anim:
#		print("change anim from: " + str(anim) +" : to : "  +str(new_anim))
		$anim.current_animation = new_anim
		anim = new_anim
	if !$anim.is_playing():
		new_anim = "idle"
#	old_pos = pos






func proc_punch(delta):
	var left = null#null= neithier left or right is pressed
	if pad_hor < -DEADZONE:
		left = true
	if pad_hor > DEADZONE:
		left = false

	var final_hits = combo_count >= (MAX_COMBO-1)

	if left != null:
		if flip_new != left:#move in opposite direction than faccing, abort punch.
			status = statedb["walking"]
			new_anim = "idle"
			has_hit = false
			combo_timer[0] = 0
			combo_count = 0
			return

	
	if Input.is_action_just_pressed("punch"):# and !final_hits:# and !reach_max:
		punch_request = true

	if punch_request:# and !final_hits:
		var crash = false
		var strength = 0.3
		if final_hits:
			if !$anim.is_playing():
				if has_hit:
					if combo_count >= (MAX_COMBO):#this is the last
#						combo_count = 0
						strength = 2.2
						crash = true
						new_anim = "combo-3"
						punch_request = false
					elif combo_count >= (MAX_COMBO-1):
						combo_count += 1
						strength = 1.3
						new_anim = "combo-2"
				throw_hit("punch", 0.5, null, crash, strength)
		elif anim != "combo-3":
#			print("normal punch requiest with anim: " +str(anim))
			punch_request = false
			if has_hit:
				combo_count += 1
				new_anim = str("combo-",(combo_count & 1))

			else:# combo_count != 0:
				new_anim = "combo-0"
			throw_hit("punch", 0.3, null, crash, strength)
#		punch_request = false


	
	

	if anim != new_anim:
		$anim.play(new_anim)
		anim = new_anim


	if !$anim.is_playing():
		if anim == "combo-3":
			combo_count = 0
		status = statedb["walking"]

	if !(combo_timer[0] > 0):
		has_hit = false
		combo_count = 0

func throw_hit(what, time, life_anim = null, crash = false, strength = 1):
	
	var dest = $sprite/attack.get_node(what)
	
	if dest == null:
		return
	
#	print("throwing :" + str(what) + ": timeout : " +str(time))
	var hitting = hit.instance()
	hitting.deep = global_transform.y
	hitting.strength = strength#how much power deal the blow
	hitting.crash = crash#force the target to crash on ground true/false
	hitting.player = self#let the know the target which player is beating
	hitting.origin = dest#location where the blow will appear Vector2()
	hitting.timeout = time#plain timeout to dissolve the blow (if nothing else dissolved first)
	hitting.lifeanim = life_anim#when player is not playing this animation, die
	hitting.z_deep = z_index#used to calculate if the blow actually hit the spot (same both player and target at same Zdepth)
	get_parent().add_child(hitting)
#	hitting.global_position = dest.global_position

func proc_jump(delta):
	check_ground(delta)
	if Input.is_action_just_pressed("punch") and (anim =="jump-landoff"):
		var flip = flip_old
		if abs(pad_hor) > 0.3:#key is pressed
#			print("detected padhor")
			if pad_hor > 0:
#				jump_linear_inertia += 1
				flip = false
			else:
#				jump_linear_inertia -= 1
				flip = true
			jump_linear_inertia = pad_hor
		if flip_old != flip:
			flip_new = flip
			new_anim ="jump-kick-rotate"
			throw_hit("jump-kick", 3, "jump-kick-rotate", true, 1.3)
		else:
			new_anim ="jump-kick"
			throw_hit("jump-kick", 3, "jump-kick", true, 1.3)
			


	if falling:
#		check_ground(delta)
		var distance = $sprite.global_position.y-$check_ground.global_position.y
#		var height = $sprite.global_position.y
#		var land_ground = $check_ground.global_position.y
#		var fall_speed = new_distance-old_distance
#		print(fall_speed)
		if distance>=-32:#jump ends
			falling = false
			anim = "idle"
			new_anim = "idle"
			$anim.current_animation = "jump-landing"
			set_collision_layer_bit(1,true)
			set_collision_mask_bit(1,true)
			set_collision_layer_bit(3,true)
			set_collision_mask_bit(3,true)
			status = statedb["walking"]
			global_position.y = jump_landground-$sprite.position.y+distance
			old_pos = global_position#hardfix for flip at end of certain jumps
	if anim != new_anim:
		$anim.current_animation = new_anim
		anim = new_anim


func proc_jump_run(delta):
	check_ground(delta)

	if Input.is_action_just_pressed("punch") and (anim == "run-jump"):
		new_anim ="jump-kick"
		throw_hit("jump-kick", 3, "jump-kick", true, 1.2)
		
	if falling:
#		check_ground(delta)
		var height = $sprite.global_position.y
		var land_ground = $check_ground.global_position.y

		if height>=jump_landground:#jump ends
			if jump_linear_inertia > 0.1:
				flip_new = false
			elif jump_linear_inertia < 0.1:
				flip_new = true
			falling = false
			anim = "jump-landing"
			new_anim = "jump-landing"
			$anim.current_animation = new_anim
			set_collision_layer_bit(1,true)
			set_collision_mask_bit(1,true)
			set_collision_layer_bit(3,true)
			set_collision_mask_bit(3,true)
			status = statedb["walking"]
			global_position.y = jump_landground-$sprite.position.y
	
	if anim != new_anim:
		$anim.current_animation = new_anim
		anim = new_anim



func proc_run_smash(delta):
	if anim != new_anim:
		$anim.current_animation = new_anim
		anim = new_anim

func _process(delta):
#	label.text = str(lv.x)
#	print(abs(lv.x))
	cur_pos = global_position
	z_index = cur_pos.y
	if status == statedb["walking"]:
#		z_index = cur_pos.y
		proc_walk()
	if status == statedb["punch"]:
#		z_index = cur_pos.y
		proc_punch(delta)
	if status == statedb["jump"]:
		z_index = old_z
		proc_jump(delta)
	if status == statedb["run-jump"]:
		z_index = old_z
		proc_jump_run(delta)
	if status == statedb["run-smash"]:
		z_index = cur_pos.y
		proc_run_smash(delta)
	
	if status != statedb["run"]:
		$sprite/run.emitting = false

	if flip_old != flip_new:
		if flip_new:
			$sprite.scale = Vector2(-1,1)
		else:
			$sprite.scale = Vector2(1,1)
		flip_old = flip_new

	if combo_timer[0] > 0:
#		print(combo_timer[0])
		combo_timer[0] -= 1*delta
	else:
		combo_timer[0] = 0

#	print(cur_pos-old_pos)
	old_pos = cur_pos
	
	
	#cleanup

	
	#services:
	service_enemy_close()

func _physics_process(delta):
	if status == statedb["walking"]:
		phy_walk()
	elif status == statedb["jump"]:
		phy_jump(delta)
	elif status == statedb["run"]:
		phy_run(delta)
	elif status == statedb["run-jump"]:
		phy_jump_run(delta)
	elif status == statedb["run-smash"]:
		phy_run_smash(delta)


func phy_run_smash(delta):
#	print("inertia: " +str(run_stop_inertia))
	run_stop_inertia = lerp( run_stop_inertia, 0,0.06)
	if abs(run_stop_inertia) < WALK_SPEED:
		status = statedb["walking"]
	lv = move_and_slide(Vector2(run_stop_inertia,0))


func phy_run(delta):
	var cur_direction = pad_hor > 0#false == left. true == right

#	if cur_direction != flip_old:
#		print("assecondation")
	if (abs(pad_hor) < 0.1) or (cur_direction == flip_old):#stop to press forward
		status = statedb["walking"]
		new_anim = "idle"
		anim = "idle"
		$anim.current_animation = "run-stop"
		old_pos = global_position
		return
	elif input_old != cur_direction:#invert direction
		status = statedb["walking"]
		return
#	if abs(lv.x) <= MIN_MOVE:
	if abs(lv.x) <= MIN_RUN:
		print("stop for slowness")
		status = statedb["walking"]
		new_anim = "idle"
		anim = "idle"
		$anim.current_animation = "run-stop"
		old_pos = global_position
		return
#	if (abs(pad_hor) < 0.1) or (input_old != cur_direction):
#		status = statedb["walking"]
#		return

	var direction = Vector2(0,0)
	if cur_direction:
		direction.x = RUN_SPEED
	else:
		direction.x = -RUN_SPEED
	direction.y = pad_ver*WALK_SPEED
	


	lv = move_and_slide(direction)

	if Input.is_action_just_pressed("jump"):
		var dir = lv.x < 0
		status = statedb["run-jump"]
		jump_y_origin = $check_ground.global_position.y
		jump_landground = jump_y_origin
		new_anim = "run-jump"
		old_z = global_position.y
		$anim.current_animation = new_anim
		jump_gravity = JUMP_STR
		set_collision_layer_bit(1,false)
		set_collision_mask_bit(1,false)
		set_collision_layer_bit(3,false)
		set_collision_mask_bit(3,false)
		falling = false
		if dir:
			flip_new = true
			jump_linear_inertia = -4
		else:
			flip_new = false
			jump_linear_inertia = 4
	elif Input.is_action_just_pressed("punch"):
		status = statedb["run-smash"]
		new_anim = "run-smash"
		if lv.x < 0:
			run_stop_inertia = -RUN_SPEED*1.5
		else:
			run_stop_inertia = RUN_SPEED*1.5
		throw_hit("jump-kick",0.3)

		return

	

	label.text = str(lv)


		




func check_ground(delta):#scan the ground to find an empty spot for the player to land
	var checking = $check_ground
	checking.global_position.y = jump_landground
	var tune_speed = 1000
	var dir = 0#assume no collision up&down until proven contrary
	var bodies_up = $check_ground/up.get_overlapping_bodies()
	var bodies_dn = $check_ground/dn.get_overlapping_bodies()
#	if bodies_up != []:#top spot is colliding, go down
#		var bypass = false
#		for n in bodies_up:#ugly attempt to make exception with enemies
#			n.is_in_group("enemies") 
#			bypass = true
#		if !bypass:
#			dir = tune_speed*delta
#	elif bodies_dn != [] or bodies_dn.has(self):
#		var bypass = false
#		for n in bodies_up:#ugly attempt to make exception with enemies
#			n.is_in_group("enemies") 
#			bypass = true
#		if !bypass:
#			dir = -tune_speed*delta
	if bodies_up != []:#top spot is colliding, go down
		dir = tune_speed*delta
	elif bodies_dn != [] or bodies_dn.has(self):
		dir = -tune_speed*delta

	else:#no collision, try return to orign y
		if jump_landground > jump_y_origin:
			dir = -tune_speed*delta*0.6
		else:
			dir = tune_speed*delta*0.6

	jump_landground += dir
	




func phy_jump(delta):
	var move = Vector2(0,0)

	move.x = jump_linear_inertia*WALK_SPEED
	if jump_gravity < 0 and !falling:#going up
		
		jump_gravity += GRAVITY

		move.y = jump_gravity
	elif !falling:#mid fly event
		falling = true
	else:
		move.y = jump_gravity
		jump_gravity = min(MAX_GRAVITY, jump_gravity+GRAVITY)

	lv = move_and_slide(move)


func phy_jump_run(delta):
	var move = Vector2(0,0)

	move.x = jump_linear_inertia*WALK_SPEED
	if jump_gravity < 0 and !falling:#going up
		
		jump_gravity += GRAVITY

		move.y = jump_gravity
	elif !falling:#mid fly event
		falling = true
	else:
		move.y = jump_gravity
		jump_gravity = min(MAX_GRAVITY, jump_gravity+GRAVITY)

	lv = move_and_slide(move)



func phy_walk(delta = null):


	var move_dir = Vector2(pad_hor*WALK_SPEED,pad_ver*WALK_SPEED)

	if Input.is_action_just_pressed("jump"):
		status = statedb["jump"]
		jump_y_origin = $check_ground.global_position.y
		old_z = global_position.y
		jump_landground = jump_y_origin
		new_anim = "jump-landoff"
		$anim.current_animation = new_anim
		jump_gravity = JUMP_STR
		set_collision_layer_bit(1,false)
		set_collision_mask_bit(1,false)
		set_collision_layer_bit(3,false)
		set_collision_mask_bit(3,false)
		falling = false
		jump_linear_inertia = pad_hor*3
		

		if pad_hor < -0.1:
			flip_new = true
		elif pad_hor > 0.1:
			flip_new = false

#	elif Input.is_action_just_pressed("debug_c"):
#		new_anim = "combo-1"
#		status = statedb["punch"]
#	elif Input.is_action_just_pressed("debug_d"):
#		new_anim = "combo-2"
#		status = statedb["punch"]
#	elif Input.is_action_just_pressed("debug_e"):
#		new_anim = "combo-3"
#		status = statedb["punch"]
#	elif Input.is_action_just_pressed("debug_f"):
#		new_anim = "combo-4"
#		status = statedb["punch"]
#	elif Input.is_action_just_pressed("debug_g"):
#		print("shooting")
#		give_enemy_spot()
	
	lv = move_and_slide(move_dir)
#	lv = move#.length()
	if abs(lv.length()) < 45:#if the moviment was too little, simply don't move
		global_position = global_position
