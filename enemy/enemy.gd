extends KinematicBody2D



const DAMAGE_BASE = 0.06
var base_health = 100


#nodes
var sprite
var player_active
var target
var healthbar
var pathfind


const DEADZONE = 0.5
const WALK_SPEED = 300

const RECOVER_FALL = 0.1

var nohit_stat = []#the status in which this entity won't take damage (when is trashed, when is dying etc...)
var recover_delay

#dynamic stats
var health = 10
var health_current = 1.0

#being hit dynamics
var hit_inertia = 0
var got_puch = false

#status control
const statedb ={"idle":0
				,"reach_position":1#target is reached
				,"reached":2
				,"attacking":3
				,"stuck":4
				,"punched":5
				,"trashed":6#push on the ground, but still alive
				,"knockout":7#dieing
				,"turntowards":8#a target must be present
#				,1:"jumping"
				}

var status = 0


const ai_statedb={"aggressive":0#freshly came into play, attempt to hit player anytime
				,"disciplinated":1#got few punches, try to pack with others
				,"defensive":2#try to hit player when busy punching someone else (also "lowhealth/coward")
				}
var ai_state = 0
var ai_timer = 0
var ai_target_focus = null#the target player we're attacking

#animation control
var anim = "idle"
var new_anim = "idle"
var lv = Vector2()#linear velocity
var old_pos = Vector2(0,0)
var flip_old = false
var flip_new = false


#interaction with player and AI
var target_distance = Vector2(0,0)
#var target_left = false
#var busy_stomp =false#
#size and scale



#dev stuff (will be erased)
var label



func get_target_walk(target = base.player,proximity = null, face_player = null):
	#face_player= null(no preference, wherever is close), face (walk preferably in front of player), backstab (cowardly walk at player back)
	if proximity == null:#when proximity is not requested, do do randomly
		proximity = randf()
	pathfind.global_position = global_position
	var reference_position = global_position
	if face_player != null:
		var dif = abs(target.global_position.x-reference_position.x)
		var player_watch_left = target.flip_old
		reference_position.x = target.global_position.x
		if face_player == "front":
			if player_watch_left:#player is watching left and we go at left
				reference_position.x -= dif
			else:#player is watching right and we go at right
				reference_position.x += dif
#			base._debug_shotmark(reference_position)
		elif face_player == "backstab":
			if player_watch_left:#player is watching left and we go at right
				reference_position.x += dif
			else:#player is watching right and we go at left
				reference_position.x -= dif
	base._debug_shotmark(reference_position)
	var to = target.give_enemy_spot(reference_position, proximity)
	pathfind.set_cast_to(to-global_position)
	pathfind.force_raycast_update()
	if pathfind.is_colliding():
		to = global_position.linear_interpolate(pathfind.get_collision_point(),0.5)#-pathfind.global_position
	issue_a_reach_position(to)

func got_knockout(who, how, strength):
	status = statedb["knockout"]
	healthbar.value = 0
	var player_pos =Vector2(who.global_position.x, who.z_index)
	var dif = global_position-player_pos
	hit_inertia = dif.normalized()*1500*strength
	var was_hit_back = dif.x > 0
	if flip_old == was_hit_back:#was facing player when crashed
		new_anim = "gotknock-back-crash"
	else:#was crashed in the back
		new_anim = "gotknock-front-crash"
		flip_new = !flip_old
#	var pos = 
	var pos =Vector2(who.global_position.x, who.z_index)
#	base._debug_shotmark(pos)

func get_hit(who, how, strength = 1, crash = false):
#	print("got hit request with: " +str(status))
	if nohit_stat.has(status):#during special status, can't get more hit from players
#		print("in spacial case, not getting hurt")
		return
	var power = (strength*DAMAGE_BASE)*100
	if power >= healthbar.value:
		got_knockout(who, how, strength)
		return
	healthbar.value -= power
	player_active = who
	if crash:
#		if who.get_node("anim").current_animation == "combo-3":
#			print("the crash is here! the stat is: "+str(status))

		var player_pos =Vector2(who.global_position.x, who.z_index)
		var dif = global_position-player_pos
		hit_inertia = dif.normalized()*2000
		var was_hit_back = dif.x > 0
		#next animation are forced becouse are used to control the process flow (bad code design, I know)
		if flip_old == was_hit_back:#was facing player when crashed
			new_anim = "gotknock-back-fall"
			anim =  "gotknock-back-fall"
			$anim.play("gotknock-back-fall")
		else:#was crashed in the back
			flip_new = !flip_old
			new_anim = "gotknock-front-fall"
			anim = "gotknock-front-fall"
			$anim.play("gotknock-front-fall")
		status = statedb["trashed"]
	else:
		status = statedb["punched"]
		got_puch = true





#func _ready():
#	call_deferred("reparent_pathfind")
#
#
func reparent_pathfind():#pathfind raycast collider is placed outside in the world
	
	pathfind = $pathfinder
	remove_child(pathfind)
	base.world.add_child(pathfind)
	pathfind.add_exception(self)
	pathfind.set_owner(base.world)
	
func _enter_tree():
	call_deferred("reparent_pathfind")#pathfind raycast is reparented with the world
#										...so if the enemy root is scaled, it don't mess with the raycast

#var target = get_node("../..")
#var source = get_node("child")
#self.remove_child(source)
#target.add_child(source)
#source.set_owner(target)

	
#	pathfind = $pathfind_ray
#	base.world.add_child(pathfind)
#	pathfind.set_owner(base.world)

	add_to_group("enemies")
	#print(typeof(base.player))#) = 19)
	nohit_stat = [statedb["knockout"],statedb["trashed"]]
	if typeof(base.player) == TYPE_ARRAY:
		pass
#		for p in base.player:
			
	else:
		base.player.get_node("check_ground/up")
		base.player.get_node("check_ground/dn")
		
	var pickup = randi()%9
#	var choice = (randi()%9 > 4)
	if pickup < 3:
#		print("a")
		$sprite.frames = base.spr_a
	elif pickup > 6:
#		print("b")
		$sprite.frames = base.spr_b
	else:
#		print("c")
		$sprite.frames = base.spr_c
		scale = Vector2(1.35,1.35)
#	if choice:
#		$sprite.frames = base.spr_hero
#	else:
#		$sprite.frames = base.spr_heroine
	sprite = $sprite
#	sprite.scale = base_scale
	set_meta("enemy",0)
	ai_target_focus = base.player


	label = $Label
	base.enemy_debug = self


func proc_underatk(delta):

	if got_puch:
		var dif = global_position-player_active.global_position
		if dif.x > 0:
			flip_new = true
		else:
			flip_new = false
		if flip_new != flip_old:
			new_anim = "gothit-back"
			got_puch = false
		else:
			new_anim = "gothit-mid"
	if !$anim.is_playing():
		if got_puch:
			got_puch = false
			return
		new_anim = "idle"
		status = statedb["idle"]

func duplicate_node():
	var template = get_node("sprite")

	var frames = template.frames
	var new_sprite = Sprite.new()
	var cur_anim=  template.get_animation()
	var texture = frames.get_frame(cur_anim, frames.get_frame_count(cur_anim)-1)
	new_sprite.set_texture(texture)
	new_sprite.global_position = template.global_position
	new_sprite.z_index = z_index
	var new_scale = scale
	if flip_old:
		new_scale.x = -(new_scale.x)
	new_sprite.scale = new_scale

	base.world.add_child(new_sprite)
#	print("texture is: " +str(texture))


func proc_trashed(delta):

	if !$anim.is_playing():
		
		if anim == "gotknock-back-fall":
#			print("phase 1")
			new_anim = "gotknock-back-land"
		elif anim == "gotknock-front-fall":
#			print("phase 1")
			new_anim = "gotknock-front-land"
		else:
#			print("trashed ended now")
			new_anim = "idle"
			status = statedb["idle"]


func _process(delta):
#	label.text = str(target_distance.length())
	z_index = global_position.y
#	$Label.text = str(lv)
	ai_control(delta)
	if status == statedb["reach_position"]:
		proc_reach_position()
	elif status == statedb["punched"]:
		proc_underatk(delta)
	elif status == statedb["trashed"]:
		proc_trashed(delta)
	elif status == statedb["turntowards"]:
		if !$anim.is_playing():
			new_anim = "idle"
			status = statedb["idle"]

	if anim != new_anim:
		$anim.play(new_anim)
		anim = new_anim
	if flip_old != flip_new:
		if flip_new:
			sprite.scale = Vector2(-1,1)
		else:
			sprite.scale = Vector2(1,1)
		flip_old = flip_new

	if Input.is_action_just_pressed("debug_b"):
#		get_target_walk(base.player)
		get_target_walk()


	
#func debug_takespot():
#
#	target = null
#	var pos = global_position
#	var shortest
#	for i in base.player.get_node("target").get_children():
#		var check = set_reach_pose(base.player, i.name)
#		if check != null:
#			var distance = (pos-check).length()
#			if shortest == null:#the first one, we just accept it
#				shortest = distance
#				target = check
#			elif shortest > distance:
#				target = check
#				shortest = distance
#
#	if target != null:
#		status = statedb["reach_position"]
#	else:#no target available, return to idle
#		target = pos
#		status = statedb["idle"]


func issue_a_reach_position(toplace = null):
#
#	if status != statedb["idle"]:
#		print("not idle")
#		return

	if toplace == null:
		target = global_position
		status = statedb["idle"]
		return
		
	var dif = (toplace-global_position).length()
	if dif < 50:
#		print("too close, ignore")
		return
	target = toplace
	status = statedb["reach_position"]

	
	return
	
	target = null
	var pos = global_position
	var shortest
	for i in base.player.get_node("target").get_children():
		var check = set_reach_pose(base.player, i.name)
		if check != null:
			var distance = (pos-check).length()
			if shortest == null:#the first one, we just accept it
				shortest = distance
				target = check
			elif shortest > distance:
				target = check
				shortest = distance

	if target != null:
		status = statedb["reach_position"]
	else:#no target available, return to idle
		target = pos
		status = statedb["idle"]


func are_there_atk_condition():
	var condition = false
	if target_distance.length() < 200:
		condition = true
#		print(target_distance.length())
#	print(target_distance)
	return condition

func ai_control(delta):
	var ai_switch = null
	
	if ai_state == ai_statedb["aggressive"]:
#every 3~7 seconds, player attempt to get in the fight right around the player
#once enough close, attack.
		if ai_timer <= 0:#take the next aggressive choice, then reset timer accordly
			var face_choice = "front"
			if (randi()%2)>0:
				face_choice= "backstab"
#			label.text = str("I'll ",face_choice,"\ndist: ",target_distance )
			get_target_walk(ai_target_focus,rand_range(0, 0.3), face_choice)
			ai_timer = randi()%7+2
			if face_choice == "front":
				label.text = "I'll face the player"
			else:
				label.text = "I'll attempt to backstab the player"
#			print("aggro now, next is: "+ str(ai_timer) + "face c is: " +str(face_choice))
			
		else:
			ai_timer -= 1*delta
		pass
	if status == statedb["idle"]:
		if (anim == "idle"):
			var at_my_left = target_distance.x >0
			if at_my_left !=flip_new:
				#animation control process, so we force it right now
				$anim.play("turn")
				new_anim = "turn"
				anim = "turn"

				flip_new = !flip_new
				status = statedb["turntowards"]
		if Input.is_action_just_pressed("debug_a"):
			target = null
			var pos = global_position
			var shortest
			
			for i in base.player.get_node("target").get_children():
				var check = set_reach_pose(base.player, i.name)
				if check != null:
					var distance = (pos-check).length()
					if shortest == null:#the first one, we just accept it
						shortest = distance
						target = check
					elif shortest > distance:
						target = check
						shortest = distance
			if target != null:
				status = statedb["reach_position"]
		if are_there_atk_condition():
			punch(base.player)
				

	elif status == statedb["reached"]:
		if global_position.x > base.player.global_position.x:
			flip_new = true
		else:
			flip_new = false
		status = statedb["idle"]
		new_anim = "idle"
	elif status == statedb["attacking"]:
		if !$anim.is_playing():
			status = statedb["idle"]
			new_anim = "idle"

#############AI FOOTER
	if ai_switch != null:
		pass#the the requested switch operation


func set_reach_pose(player, pose):
	var to_reach = player.get_node("target/"+str(pose))
	var occluded = (to_reach.get_overlapping_bodies() != [])
	
	if occluded:
		return null

	return to_reach.global_position



func check_direction():
	var walk_dir = lv.normalized()
	var dir = "side"
	if walk_dir.x > 0:
		flip_new = false
	else:
		flip_new = true

	if walk_dir.y > DEADZONE:
		dir = str(dir,"dn")
	elif walk_dir.y < -DEADZONE:
		dir = str(dir,"up")
	return dir


func proc_reach_position():
	var pos = global_transform.origin
	var direction = check_direction()
	if direction == null:
		new_anim = "idle"
	elif direction == "side":
		new_anim = "walk-side"
	elif direction.ends_with("up"):
		new_anim = "walk-sideup"
	elif direction.ends_with("dn"):
		new_anim = "walk-sidedn"
#

	if !$anim.is_playing():
		new_anim = "idle"
	old_pos = pos



func _physics_process(delta):
#	print(status)
	if status == statedb["reach_position"]:
		phy_reach_position()
#	elif status == statedb["punched"]:
		
#		 hit_inertia = (global_position-player_active.global_position).normalized()
#		phy_being_hit("punch",delta)
	elif status == statedb["trashed"]:
		phy_trashed(delta)
	elif status == statedb["knockout"]:
		phy_knockout(delta)

func phy_trashed(delta):
	hit_inertia = hit_inertia.linear_interpolate( Vector2(0,0),0.1)
	lv = move_and_slide(hit_inertia)
#	print(lv.length())

func vanish():
#	print("killing me: " +str(name))
	remove_from_group("enemies")
	
#	var new_stuff = load("res://enemy/enemy.tscn").instance()
#	base.world.add_child(new_stuff)
	
	
	pathfind.queue_free()
	queue_free()

func phy_knockout(delta):
	hit_inertia = hit_inertia.linear_interpolate( Vector2(0,0),0.1)
	lv = move_and_slide(hit_inertia)
	if !$anim.is_playing():
		if anim == "gotknock-back-crash":
			duplicate_node()
			vanish()
		elif new_anim == "gotknock-front-crash":
			duplicate_node()
			vanish()

#func phy_being_hit(how,delta):
#	label.text = "phy. beinghit"
#	if how == "punch":
#		return
#
#		var dif = global_position-player_active.global_position
#		if dif.x > 0:
#			flip_new = true
#		else:
#			flip_new = false
#		hit_inertia = hit_inertia.linear_interpolate( Vector2(0,0),0.1)
#		lv = move_and_slide(hit_inertia)
#		if hit_inertia.length() <= 0.01:
#			print("reset for inertia")
#			status = statedb["idle"]
#			new_anim = "idle"

func phy_reach_position(delta = null):
	var pos = global_position
	var move_dir = target-pos#TOFIX,error case Nil sometime (when AI is stuck and issue an auto-retry)
	if move_dir.length() < 5.0:
		status = statedb["reached"]
		return
	
#	var pos = global_position
#	var move_dir = target-pos#TOFIX,error case Nil sometime (when AI is stuck and issue an auto-retry)
#	if move_dir.length() < 5.0:
#		status = statedb["reached"]
#		var dif = base.player.global_position-global_position
#		var at_my_left = false
#		if dif.x < 0:
#			at_my_left = true
#		if at_my_left !=flip_new:
#			#animation control process, so we force it right now
#			$anim.play("turn")
#			new_anim = "turn"
#			anim = "turn"
#
#			flip_new = !flip_new
#			status = statedb["turntowards"]
#		return
	move_dir = move_dir.normalized()*WALK_SPEED
	lv = move_and_slide(move_dir,Vector2(0,0),false, true,4,0.1)
	if lv.length() < 5:
		status = statedb["reached"]
		return
	
#Vector2 linear_velocity, Vector2 floor_normal=Vector2( 0, 0 ), bool infinite_inertia=true, bool stop_on_slope=false, int max_bounces=4, float floor_max_angle=0.785398 
	
	if lv.length() < 45:#if the moviment was too little (entity stuck), abort
		var new_pos = Vector2(randi()%30,randi()%30)
#		print(new_pos)
		issue_a_reach_position(global_position+new_pos)

func punch(player):
	var dist = (player.global_position.x-global_position.x)< 0#left is true
	if dist != flip_old:#not watching, ignore
		return

	new_anim = "punch"
	if !dist:
		flip_new = false
	else:
		flip_new = true
	status = statedb["attacking"]

func _on_detect_player_body_entered(body):
	pass
#	print("nope!")
#	if body.has_meta("player"):
#		punch(body)



func _on_personal_violation(area):#another enemy is too close
	if status != statedb["idle"]:
#		print("not idle")
		return
	var violator = area.get_parent()
	if !violator.is_in_group("enemies"):
		print("not a fellow thugs, ignore")
		return
#	if busy_stomp:
#		return
	var dif = (violator.global_position-global_position)/2#relative distance
	issue_a_reach_position(global_position-dif)
#	busy_stomp
#	var get_away_from_it = personal_space_violator.global_position+gl
#
#	print(area.get_parent().name)
