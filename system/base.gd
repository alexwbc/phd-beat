extends Node

var player
var players = []
var spr_hero
var spr_heroine
var spr_a
var spr_b
var spr_c

var enemy_debug

var world

func _enter_tree():
	randomize()
	world = get_node("/root/main")
	spr_a = preload("res://player/hero/animation.tres")
	spr_b = preload("res://player/heroine/animation.tres")
	spr_c = preload("res://enemy/bigguy/animation.tres")





func _debug_shotmark(location):
	var shotmark = load("res://draft/shotmark.tscn").instance()
	shotmark.global_position = location
	get_node("/root/main").add_child(shotmark)